<?php
    date_default_timezone_set('Europe/Moscow'); // Set timezone correctly!

    $tour_id = intval($_GET['tid']);

    $auth_token = '111111'; // Secret authorization token

    $api_tour_url = 'http://integration-service:7001/id1/api/v1/tour/'.$tour_id;

    $ctx = stream_context_create(array('http'=>
        array(
            'timeout' => 5,  // 5 Seconds is maximum timeout
            'header' => 'Authorization: Bearer '.$auth_token, // Use bearer authorization
        )
    ));

    set_error_handler(
        function ($severity, $message, $file, $line) {
            throw new ErrorException($message, $severity, $severity, $file, $line);
        }
    );

    try {
        $json = file_get_contents($api_tour_url, false, $ctx);
        $tour = json_decode($json);
        $take_defaults = '';
    }
    catch (Exception $e) {
        // Errors handling
        // The default value if server returned errors or request timed out
        $tour =
            (object) array(
                'code' => 'XXXX',
                'name' => 'Обзорная экскурсия по Петербургу, например',
                'region' => 'Санкт-Петербург, например',
                'type' => 'Продажа, например',
                'details' =>
                    [
                        array(
                            'tour_date' => new DateTime('2019-04-19 12:50', new DateTimeZone('GMT')),
                            'code' => 'YYYY',
                            'name' => 'Обзорная экскурсия по Петербургу, например',
                            'prices' =>
                                array(
                                    'adult_rus' => 1000,
                                    'school_rus' => 900
                                )
                        ),
                    ]
            );
        $take_defaults = '<br/><br/>Данные по этой экскурсии не были получены от сервиса, поэтому загружено состояние по умолчанию<br/><br/>';
    }

    restore_error_handler();

    /** Translates the date in GMT to Moscow time zone and formats the date.
     */
    function format_moscow_time($date_value){
        $moscow_tz = 3600 * 3;
        return gmdate("d.m.Y H:i", $date_value + $moscow_tz);
    }

?>
<html>
<style>
    td, th {
        border: 1px solid gray;
        padding: 5px;
    }
    .aright {
        text-align: right;
    }
    table {
        border-spacing: 0;
        border-collapse: collapse;
    }
</style>
<body>

    <a href="https://gitlab.com/sank_unitour/unitour-php-example/blob/master/www/unitour/tour.php">Source code on gitlab</a>
    <?=$take_defaults?>
    <table>
        <tr>
            <th class="aright">Код</th>
            <td><?=$tour->code?></td>
        </tr>
        <tr>
            <th class="aright">Название</th>
            <td><?=$tour->name?></td>
        </tr>
        <tr>
            <th class="aright">Регион</th>
            <td><?=$tour->region?></td>
        </tr>
        <tr>
            <th class="aright">Тип</th>
            <td><?=$tour->type?></td>
        </tr>
    </table>
    <h1>Расписание</h1>
    <table>
        <tr>
            <th>№</th>
            <th>Дата</th>
            <th>Код</th>
            <th>Название</th>
            <th>Цена взрослый</th>
            <th>Цена школьный</th>
        </tr>
<?php
        $i = 0;
        foreach($tour->details as $detail){
            $i += 1;
?>
        <tr>
            <td class="aright"><?=$i?></td>
            <td><?=format_moscow_time($detail->tour_date)?></td>
            <td><?=$detail->code?></td>
            <td><?=$detail->name?></td>
            <td class="aright"><?=$detail->prices->adult_rus?></td>
            <td class="aright"><?=$detail->prices->school_rus?></td>
        </tr>
<?php
        }
?>
    </table>
    <br/>
    <a href="javascript:history.back();">Назад</a?

</body>
</html>
