<?php
    $page_num = intval(empty($_GET['page_num']) ? '1': $_GET['page_num']);

    $auth_token = 'your-token'; // Secret authorization token

    $api_list_url = 'http://host-unitour-service/id1/api/v1/tours?sort_cols=operator,id&page_num='.$page_num;

    $ctx = stream_context_create(array('http'=>
        array(
            'timeout' => 5,  // 5 Seconds is maximum timeout
            'header' => 'Authorization: Bearer '.$auth_token, // Use bearer authorization
        )
    ));

    $json = file_get_contents($api_list_url, false, $ctx);

    $tours = json_decode($json);

    // Пример фильтрации выборки по дню (по примеру из API день 01.06.2019)

    // Перевод даты в timestamp в GMT timezone
    function date_to_utc($date_value) {
        $date_value->setTimezone(new DateTimeZone('Europe/London'));
        return $date_value->getTimestamp ();
    }
    // Нас интересует фильтрация по дню 1 июня 2019
    $test_day_filter = new DateTime('2019-06-01', new DateTimeZone('Europe/Moscow'));
    $test_day_filter = date_to_utc($test_day_filter);
?>
<html>
<style>
    td, th {
        border: 1px solid gray;
    }
    table {
        border-spacing: 0;
        border-collapse: collapse;
    }
</style>
<body>

<?php
    if($page_num>1) {
?>
    <a href="index.php?page_num=<?=$page_num-1?>">Prev page</a>
<?php
    }
?>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="index.php?page_num=<?=$page_num+1?>">Next page</a>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="https://gitlab.com/sank_unitour/unitour-php-example/blob/master/www/unitour/index.php">Source code on gitlab</a>
    <br/>
    <br/>
    <a href="tour.php?tid=-100">Переход на экскурсию, которой нет в базе</a>
    <br/>
    <a href="index.php?day=><?=$test_day_filter?>">Фильтр по дате экскурсии в расписании</a>
    <br/>
    <br/>
    <table cellspasing="0" cellspadding="0">
        <tr>
            <th>Код</th>
            <th>Оператор</th>
            <th>Название</th>
            <th>Регион</th>
            <th>Тип</th>
        </tr>
    <?php
        foreach($tours as $tour){
    ?>
        <tr>
            <td><a href="tour.php?tid=<?=$tour->id?>"><?=$tour->id?></a></td>
            <td><?=$tour->operator_name?></td>
            <td><?=$tour->name?></td>
            <td><?=$tour->region?></td>
            <td><?=$tour->type?></td>
        </tr>
    <?php
        }
    ?>
    </table>

</body>
</html>
